from app.config.extractors.csv import csv_extractor
from app.config.extractors.metaweather import metaweather_extractor

SOURCES = [
    {
        "name": "metaweather",  
        "extractor": metaweather_extractor
    },
    {
        "name": "csv",
        "extractor": csv_extractor
    }
]