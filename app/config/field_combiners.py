from app.utils.FieldCombiner import FieldCombiner

FIELD_COMBINERS = [
    FieldCombiner( 'bars_per_capita',    ["bars", "population"],    lambda x, y: x / y ),
    FieldCombiner( 'museums_per_capita', ["museums", "population"], lambda x, y: x / y )
]
