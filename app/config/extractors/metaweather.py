from tornado.httpclient import AsyncHTTPClient

import datetime
import json

METAWEATHER_WOEID_URL = 'https://www.metaweather.com/api/location/search/?query={city_name}'
METAWEATHER_URL       = 'https://www.metaweather.com/api/location/{woeid}/{year}/{month}/{day}'

async def metaweather_extractor(cities):
    result = {}
    http_client = AsyncHTTPClient()
    for city in cities:
        try:
            response = await http_client.fetch(METAWEATHER_WOEID_URL.format(city_name=city))
        except Exception as e:
            print("Error: %s" % e)
            return {}
    
        data = json.loads(response.body)
        woeid = None
        for entry in data:
            if entry['location_type'] == 'City' and entry['title'].lower() == city:
                woeid = entry['woeid']
                break

        if not woeid:
            print(f"Location Not Found: {city}")
            return {}

        now = datetime.datetime.now()

        try:
            response = await http_client.fetch( METAWEATHER_URL.format(woeid=woeid, year=now.year, month=now.month, day=now.day) )
        except Exception as e:
            print("Error: %s" % e)
            return {}
        
        data = json.loads(response.body)[0]
        result[city] = {
            "humidity"                    : data['humidity'],
            "current_temperature"         : data['the_temp'],
            "visibility"                  : data['visibility'],
            "wind"                        : data['wind_speed'],
            "current_weather_description" : data['weather_state_name']
        }
    return result