import csv

INDEX_CITY               = 1
INDEX_COUNTRY            = 2
INDEX_POPULATION         = 3
INDEX_BARS               = 4
INDEX_MUSEUMS            = 5
INDEX_PUBLIC_TRANSPORT   = 6
INDEX_CRIME_RATE         = 7
INDEX_AVERAGE_HOTEL_COST = 8

async def csv_extractor(cities):
    city_set = set(cities)
    result = {}
    with open('data/european_cities.csv') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
            row_city = row[INDEX_CITY].lower()
            if row_city in city_set:
                city_set.discard(row[INDEX_CITY])
                result[row_city] = {
                    'population'         : int(row[INDEX_POPULATION]), 
                    'bars'               : int(row[INDEX_BARS]), 
                    'museums'            : int(row[INDEX_MUSEUMS]), 
                    'public_transport'   : int(row[INDEX_PUBLIC_TRANSPORT]), 
                    'crime_rate'         : int(row[INDEX_CRIME_RATE]), 
                    'average_hotel_cost' : int(row[INDEX_AVERAGE_HOTEL_COST])
                }

            if not city_set:
                break
    return result
