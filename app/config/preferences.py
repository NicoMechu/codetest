from app.utils.preference import GaussianPreference, UniformPreference, OptionsPreference

# In this file is described the list of preferences of the user. 

PREFERENCES = [

    GaussianPreference('current_temperature', 8, best=23        , standar_distribution=15        ),
    GaussianPreference('population'         , 2, best=1_500_000 , standar_distribution=2_000_000 ),
    GaussianPreference('bars_per_capita'    , 7, best=0.0018    , standar_distribution=0.0015    ),

    UniformPreference('public_transport' , 7, max=10 , min=1  ),
    UniformPreference('crime_rate'       , 9, max=1  , min=10 ),

    OptionsPreference('current_weather_description', 5, options= {
            'Snow'         : 8.8,
            'Sleet'        : 3.1,
            'Hail'         : 2.1,
            'Thunderstorm' : 0.1,
            'Heavy Rain'   : 4.2,
            'Light Rain'   : 5.1,
            'Showers'      : 0.1,
            'Heavy Cloud'  : 6.4,
            'Light Cloud'  : 8.7,
            'Clear'        : 10,
    } ),
]

