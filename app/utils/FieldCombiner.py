class FieldCombiner:
    '''
    Represent the definition of the combined fields. 
    This enables combining features in order to generate more usefull information.
    (E.g combining area and population to get the density of a city)
    '''

    def __init__(self, name, fields, combiner_func):
        '''
        The field list must be in the same order as used in the combiner function
        '''
        self._name = name
        self._fields = fields
        self._combiner_func = combiner_func

    def compute(self, city):
        '''
        Calculates the combined field of the city with the given parameters, 
        returns a duple of the name of the field and the value
        '''
        if any([ city.get_field(field) == None for field in self._fields ]): return None, None

        value = self._combiner_func(*[city.get_field(field) for field in self._fields])
        return self._name, value