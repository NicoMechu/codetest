import scipy.stats
from abc import ABC, abstractmethod


class Preference(ABC):
    '''
    This is an abstract class that represents a user preference. 
    The attribute field is the name of the city field to be evaluated
    The attribute relevance is an integer form 0 to 10 which express the relevance of that preference.
    '''
    
    def __init__(self, field, relevance): 
        self._field = field
        self.relevance = relevance

    @abstractmethod
    def score(self, city):
        pass

class GaussianPreference(Preference):
    '''
    The GaussianPreference is a subClass of preference base on gaussian distribution.
    The mean value corresponds to the best possible value of this field.
    Later, the score is calculated based on how far is the value form the "best value" (mean)
    '''
    def __init__(self, field, relevance, best, standar_distribution): 
        super().__init__(field, relevance)
        self._curve = scipy.stats.norm(best, standar_distribution)

    def score(self, city):
        # This returns a number from 0 to 10, where 10 is the exact mean and 
        # decrements as the value gets further
        value = city.get_field(self._field)
        return (0.5 - abs(0.5 - self._curve.cdf(value))) * 2 * 10 if value != None else None

class UniformPreference(Preference):
    '''
    The UniformPreference is a subClass of Preference based on a uniform distribution 
    It requires the max and min (or best and worse)  values to calculate the score.
    If is needed to use it reversed (for example for crime_rate where 10 is bad and 1 is good), 
    just adapt max to the lower number and min to de bigest (for crime_rate max would be 1 and min 10, which 
    is coherent with best and worse value)
    '''
    def __init__(self, field, relevance, min, max): 
        super().__init__(field, relevance)
        self._min = min
        self._difference = max - min

    def score(self, city):
        value = city.get_field(self._field)
        return 10 * (value - self._min) / self._difference if value != None else None

class OptionsPreference(Preference):
    '''
    The OptionsPreference is a subClass of Preference based on a discret list of options.
    in this case, a exhaustive dictionary of options is needed.
    '''
    def __init__(self, field, relevance, options): 
        super().__init__(field, relevance)
        self._options = options
    
    def score(self, city):
        value = city.get_field(self._field)
        return self._options.get(value, None)
