import json

from app.config.sources import SOURCES
from app.config.field_combiners import FIELD_COMBINERS
from app.config.preferences import PREFERENCES
from app.utils.City import City

async def get_city_info(city):
    '''
    This function act as a wrapper for 'get_cities_info' when there is only one city
    '''
    city_info = await get_cities([city], with_rank=False, data=True)
    return city_info[city]

async def get_cities(city_names, with_rank=True, data=False):
    '''
    This function is in charge of gathering the info of several cities. 
    It uses the functions defined in extractors.py which optimize the recollection of information for several cities at the same time if is possible. 
    '''
    cities = { name: City(name) for name in city_names }

    # Gather information from sources (asynchronously)
    promises = [source["extractor"](city_names) for source in SOURCES]
    for promise in promises:
        result = await promise
        for name in city_names:
            cities[name].update(result.get(name, {}))

    # Transform cities into a list
    cities = cities.values()

    for city in cities:
        # Compute combined fields for each city
        for field_comb in FIELD_COMBINERS:
            city.compute(field_comb)

        # Calculate the score for each city
        city.get_score(PREFERENCES)

    # Calculate rank
    if with_rank:
        City.set_ranks(cities)

    return { city.name: city.to_dict(with_data=data) for city in cities }