class City:
    '''
    The City class acts as a Builder which accumulates all the gathered information, 
    calculate score and set rank. Then present all the information as a python dictionary.
    '''

    @staticmethod
    def set_ranks(cities):
        '''
        Set rank number to every city in an array of cities. 
        This is done sorting the array and setting the rank as the position in this sorted array
        '''
        sorted_cities = sorted(cities, key = lambda city: city.score or 0 , reverse = True)
        for idx, city in enumerate(sorted_cities):
            city.set_rank(idx + 1)

    def __init__(self, city_name):
        self.name = city_name
        self.data = {}
        self.score = None
        self.rank  = None

    def update(self, new_data):
        '''
        given a dictionary with new information saves the entries in "data"
        '''
        self.data.update(new_data)

    def get_field(self, field_name):
        return self.data.get(field_name, None)

    def compute(self, combined_field):
        key, value = combined_field.compute(self)
        if key != None:
            self.data[key] = value

    def get_score(self, preferences):
            '''
            Calculates the score of the city based on the Weighted Average of the score of each relevant field
            '''
            weighted_sum = 0
            relevance = 0

            for pref in preferences:
                score = pref.score(self) 
                if score != None:
                    weighted_sum += score * pref.relevance
                    relevance    += pref.relevance

            # The weighted average if there is at least one preference, return None otherwise
            self.score = weighted_sum / relevance if relevance > 0 else None

    def set_rank(self, rank):
        self.rank = rank

    def to_dict(self, with_data):
        result = {'city_name': self.name}

        if with_data:
            result.update(self.data)

        if self.score != None:
            result['city_score'] = self.score

        if self.rank != None:
            result['city_rank'] = self.rank

        return result
