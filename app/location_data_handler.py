from tornado.web import RequestHandler
from http import HTTPStatus
import json
from app.utils import helpers
import re

class LocationHandler(RequestHandler):

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    async def get(self, *args, **kwargs):
        """
        Retrieve information about a city.
        :param args:
        :param kwargs:
        :return:
        """

        city_name = args[0]
        if not re.match(r'\w+', city_name):
            self.set_status(HTTPStatus.BAD_REQUEST)
            self.write({'Error': 'Bad request' })

        city_info = await helpers.get_city_info(city_name.lower())
        self.set_status(HTTPStatus.OK)
        self.write(city_info)
