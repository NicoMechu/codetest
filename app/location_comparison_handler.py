from tornado.web import RequestHandler
from http import HTTPStatus
import json
import re

from app.utils import helpers


class LocationComparisonHandler(RequestHandler):

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    async def get(self, *args, **kwargs):
        """
        Retrieves information about multiple cities, rates them and returns a ranking and score for each city.
        :param args:
        :param kwargs:
        :return:
        """
        cities = args[0].split(',')
        for city in cities:
            if not re.match(r'\w+', city):
                self.set_status(HTTPStatus.BAD_REQUEST)
                self.write({'Error': 'Bad request' })

        cities_info = await helpers.get_cities([city.lower() for city in cities], data=False)
        
        response = {'city_data': cities_info}
        self.set_status(HTTPStatus.OK)
        self.write(response)